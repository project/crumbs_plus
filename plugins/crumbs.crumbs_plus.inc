<?php

/**
 * @file
 *   Include file
 *
 *
 * @developers:
 *    Rafal Wieczorek <kenorb@gmail.com>
 */

/**
 * Implementation of class hook class_CrumbsParentFinder
 * on the behalf of crumbs_plus module.
 */
class crumbs_plus_class_CrumbsParentFinder {

  function getRuleTitle() {
    return t('Crumbs Plus');
  }
  function preprocessPage(&$vars, $trail) {
    // $vars['breadcrumb'] does already contain the breadcrumb built by crumbs.
    $breadcrumb = crumbs_plus_build_breadcrumb($trail);

    $vars['breadcrumb'] = theme('breadcrumb', $breadcrumb);
  }

  /*
      return TRUE, if referer host does exists
  */
  function isRefererExists() {
    return !empty($_SERVER['HTTP_REFERER']);
  }

  /*
      return TRUE, if referer host is external
  */
  function isRefererExternal() {
    return (!empty($_SERVER['HTTP_REFERER']) && parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) != $_SERVER['HTTP_HOST']);
  } 

}

/**
 * Implementation of class which extending crumbs module options
 * such as path, nolink, etc.
 */
class crumbs_plus_class_CrumbsOptions {

  protected $item;

  /** Constructor **/ 
  public function __construct($item) {
    $this->item = $item;
  } 

  public function __sleep() {
    global $crumbs_plus_callbacks;
    $crumbs_plus_callbacks[] = $this->callback = get_class($this->item) . "(" . print_r($this, TRUE) . ")"; // save callback which modify the crumb for debugging purpose
    return array('title', 'path', 'nolink', 'callback', 'replace');
  }

  /*
    Return all crumb callbacks
  */
  static function getCallbacks() {
    global $crumbs_plus_callbacks;
    return (array)$crumbs_plus_callbacks;
  }

  /*
    Load saved path
      - if class property is provided and exist, return value of the property within the class
      - if class property is provided and does not exist, return default value of the property
      - if class property is not provided, return the whole item object
  */
  static function loadItem($property = NULL, $default = NULL) {
    global $crumbs_plus_item;
    return ($crumbs_plus_item && property_exists($crumbs_plus_item, $property) ? $crumbs_plus_item->$property : ($property ? $default : $crumbs_plus_item));
  }

  /*
    Save item
  */
  static function saveItem($item) {
    global $crumbs_plus_item;
    $crumbs_plus_item = $item;
  }

  /*
    Remove saved item
  */
  static function removeItem($key = NULL) {
    global $crumbs_plus_item;
    if ($key) {
      unset($crumbs_plus_item->$key);
    } else {
      unset($crumbs_plus_item);
    }
  }

}

